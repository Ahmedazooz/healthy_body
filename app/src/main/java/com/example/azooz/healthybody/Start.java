package com.example.azooz.healthybody;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.azooz.healthybody.R;

public class Start extends Activity {
    Double Length,Weight,BMI;
    EditText weight;
    EditText length;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        weight =(EditText)findViewById(R.id.EdWeight);
        length=(EditText)findViewById(R.id.EdLength);
    }
    public void OClick(View v) {
        Weight = Double.parseDouble(weight.getText().toString());
        Length = Double.parseDouble(length.getText().toString());

        Length=Length/100;
        BMI =  (Weight) /(  Length *   Length);

        if (BMI < 16)
        {

            Intent k= new Intent(this, First.class);
            startActivity(k);
        }
        else if (BMI < 18)
        {
            Intent j= new Intent(this, Second.class);
            startActivity(j);
        }
        else if ( BMI < 24)
        {
            Intent c = new Intent(this, Third.class);
            startActivity(c);
        }
        else if (BMI < 29)
        {
            Intent l = new Intent(this, Four.class);
            startActivity(l);
        }
        else if (BMI < 35)
        {
            Intent m = new Intent(this, Five.class);
            startActivity(m);
        }
        else if (BMI > 35)
        {
            Intent n = new Intent(this, Six.class);
            startActivity(n);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
